//basic Input-Output stream
#include <iostream>
#include <stdio.h>
#include <iomanip>

//OpenGL Extension Wrangler Library
//Link : http://glew.sourceforge.net/
#include <GL/glew.h>

//Simple DirectMedia Layer is a cross-platform development library
//designed to provide low level access to audio, keyboard, mouse, joystick, 
//and graphics hardware via OpenGL
//Link : https://www.libsdl.org/
#include <SDL.h>
//SDL Wrapper for OpenGL
#include <SDL_opengl.h>
//SDL extession library for AUDIO support
#include <SDL_mixer.h>

//ImGui
//dear imgui (AKA ImGui), is a bloat-free graphical user interface library for C++. 
//It outputs vertex buffers that you can render in your 3D-pipeline enabled application. 
//It is fast, portable, renderer agnostic and self-contained (no external dependencies).
//Link : https://github.com/ocornut/imgui
#include <imgui.h>

//Implementation of ImGui based on SDL windowing system and OpenGL (shader based only). 
//Must be included at all times, else you have to write your own wrapper which can be difficult. 
//Just a tiny bit modified , most of it is found on the link below. 
//Link : https://github.com/ocornut/imgui/tree/master/examples/sdl_opengl3_example
#include <ImGUI/imgui_impl_sdl.h>
#include <ImGUI/imgui_impl_opengl3.h>

// GLM lib
// http://glm.g-truc.net/api/modules.html
#define GLM_SWIZZLE
#define GLM_FORCE_INLINE
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/random.hpp>

//local
#include "glGA/glGAHelper.h"
#include "glGA/glGAMesh.h"


void print_mat4(glm::mat4 m) {

	for (int row = 0; row < 4; row++){
		std::cout << "|";
		for (int column = 0; column< 4; column++) {
			std::cout <<std::setw(9) << m[row][column] << " ";
		}
		std::cout << "|" <<std::endl;
	}
}


int main(int, char**)
{   
	//Coordinates
	double X = 3.0;
	double Y = 1.0;
	double Z = 1.0;
	
	//---Assignment 1.a
	std::cout << "1.a:" << std::endl;

	//Column Major transformation
	glm::mat4 transformationMatrixColumn(
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		X,   Y,   Z,   1.0
	);
	
	//Row Major transformation
	glm::mat4 transformationMatrixRow(
		1.0, 0.0, 0.0, X,
		0.0, 1.0, 0.0, Y,
		0.0, 0.0, 1.0, Z,
		0.0, 0.0, 0.0, 1.0
	);

	//Translation vector
	glm::vec3 point(1.0, 2.0, 3.0);

	// print a.1 matrices
	glm::mat4 columnResult = glm::translate(transformationMatrixColumn, point);
	glm::mat4 rowResult = glm::translate(transformationMatrixRow, point);
	
	std::cout << "Column Major Translation Matrix" << std::endl;
	print_mat4(transformationMatrixColumn);

	std::cout << "Column Major Result Matrix" << std::endl;
	print_mat4(columnResult);

	std::cout << "Row Major Translation Matrix" << std::endl;
	print_mat4(transformationMatrixRow);

	std::cout << "Row Major Result Matrix" << std::endl;
	print_mat4(rowResult);

	//---Assignment 1.b
	std::cout << "1.b:" << std::endl;
	
	//Translate
	std::cout << "Translation matrix: " << std::endl;
	glm::mat4 trans_mat = glm::translate(glm::mat4(),glm::vec3(3,0,3));
	print_mat4(trans_mat);

	//Rotate
	std::cout << "Rotation matrix: " << std::endl;
	glm::mat4 rotate_mat = glm::rotate(trans_mat, 45.f, glm::vec3(0, 1, 0));
	print_mat4(rotate_mat);

	//Scale
	std::cout << "Scale matrix: " << std::endl;
	glm::mat4 scale_mat = glm::scale(rotate_mat, glm::vec3(3));
	print_mat4(scale_mat);

	//---Assignment 1.c
	std::cout << "1.c:" << std::endl;
	std::cout << "Projection matrix: " << std::endl;
	glm::mat4 projection_mat = glm::perspective(45.f, 1280.f / 720.f, 0.1f, 100.f);
	print_mat4(projection_mat);

	return 0;
}