#version 150 core

in  vec4 vPosition;
in  vec4 vColor;
out vec4 color;

uniform mat4 modelMatrix;
uniform mat4 translate;
uniform mat4 rotate;
uniform mat4 scale;
uniform mat4 project;
uniform mat4 view;

void main()
{
	//Uncomment if you want to see the basic cube translated.
    gl_Position = project * view  * scale * rotate * translate * vPosition;

	//gl_Position = vPosition;
    color = vColor;
}
