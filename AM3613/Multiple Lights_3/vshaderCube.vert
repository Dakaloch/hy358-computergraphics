#version 150 core

in  vec4 vPosition;
in	vec3 vNormal;

out vec3 fN;	//normal
out vec3 fE;	//position
out vec3 fL[2];	//lighting


uniform mat4 project;
uniform mat4 modelView;

struct Lights
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;

	float intensity;
};

uniform Lights light[2];


void main()
{
    
	fN = vNormal;
	fE = vPosition.xyz;
	
	for(int i = 0; i < 2; i++){
		fL[i] = light[i].position.xyz;

		if(light[i].position.w != 0.0){
			fL[i] = light[i].position.xyz - vPosition.xyz;
		}
	}

	gl_Position = project * modelView * vPosition;
}