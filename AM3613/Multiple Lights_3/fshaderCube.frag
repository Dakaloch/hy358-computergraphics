#version 150 core

in vec3 fN;
in vec3 fL[2];
in vec3 fE;

//uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform float Shininess;
//uniform mat4 modelView;
///uniform vec4 LightPosition;
//uniform float	Intensity;

struct Lights
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;

	float intensity;
};


uniform Lights light[2];

struct MaterialProperties
{

   vec4 ambient;   
   vec4 diffuse;   
   vec4 specular;   
   float shininess; 
};

uniform MaterialProperties cube;

void main() 
{ 
	vec3 N = normalize(fN);
	vec3 E = normalize(fE);
	
	for(int i = 0; i < 2; i++){
		vec3 L = normalize(fL[i]);

		vec3 H = normalize(L + E);
		vec4 ambient = cube.ambient * light[i].ambient;

		float Kd = max(dot(L, N), 0.0);
		vec4 diffuse = Kd * cube.diffuse * light[i].diffuse;

		float Ks = pow(max(dot(N, H), 0.0), cube.shininess);
		vec4 specular = Ks * cube.specular * light[i].specular;

		//discard the specular highlight if the light's behind the vertex
		if(dot(L, N) < 0.0){
			specular = vec4(0.0, 0.0, 0.0, 1.0);
		}

		gl_FragColor = light[i].intensity * (ambient + diffuse + specular);
		gl_FragColor.a = 1.0;
	}
} 