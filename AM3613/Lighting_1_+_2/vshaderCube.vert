#version 150 core

in  vec4 vPosition;
in	vec3 vNormal;

out vec3 fN;	//normal
out vec3 fE;	//position
out vec3 fL;	//lighting


uniform mat4 project;
uniform mat4 modelView;
uniform vec4 LightPosition;

void main()
{
    
	fN = vNormal;
	fE = vPosition.xyz;
	fL = LightPosition.xyz;

	if(LightPosition.w != 0.0){
		fL = LightPosition.xyz - vPosition.xyz;
	}

	gl_Position = project * modelView * vPosition;
}