//
//  main.cpp
//  basicCube
//
//  Created by George Papagiannakis on 23/10/12.
//  Copyright (c) 2012 University Of Crete & FORTH. All rights reserved.
//

//Marios Kanakis - 22/06/2016 
//Implementing basicGUI with SDL2 + ImGUI + OpenGL 

//Credits for the Music: http://www.bensound.com/royalty-free-music

//basic Input-Output stream
#include <iostream>
#include <stdio.h>

//OpenGL Extension Wrangler Library
//Link : http://glew.sourceforge.net/
#include <GL/glew.h>

//Simple DirectMedia Layer is a cross-platform development library
//designed to provide low level access to audio, keyboard, mouse, joystick, 
//and graphics hardware via OpenGL
//Link : https://www.libsdl.org/
#include <SDL2/SDL.h>
//SDL Wrapper for OpenGL
#include <SDL2/SDL_opengl.h>
//SDL extession library for AUDIO support
#include <SDL2/SDL_mixer.h>

//ImGui
//dear imgui (AKA ImGui), is a bloat-free graphical user interface library for C++. 
//It outputs vertex buffers that you can render in your 3D-pipeline enabled application. 
//It is fast, portable, renderer agnostic and self-contained (no external dependencies).
//Link : https://github.com/ocornut/imgui
#include <ImGUI/imgui.h>

//Implementation of ImGui based on SDL windowing system and OpenGL (shader based only). 
//Must be included at all times, else you have to write your own wrapper which can be difficult. 
//Just a tiny bit modified , most of it is found on the link below. 
//Link : https://github.com/ocornut/imgui/tree/master/examples/sdl_opengl3_example
#include <ImGUI/imgui_impl_sdl.h>
#include <ImGUI/imgui_impl_opengl3.h>


// GLM lib
// http://glm.g-truc.net/api/modules.html
#define GLM_SWIZZLE
#define GLM_FORCE_INLINE
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
#ifndef INDEX_BUFFER
#define INDEX_BUFFER        0
#endif
#ifndef POS_VB
#define POS_VB              1
#endif
#ifndef NORMAL_VB
#define NORMAL_VB           2
#endif
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/random.hpp>

//local
#include "glGA/glGAHelper.h"
#include "glGA/glGAMesh.h"

// Our function prototypes
//Starts up SDL , creates window and initializes OpenGL
bool			init();

//Initializes rendering program and clear color
bool			initGL();

//Initializes ImGui
bool			initImGui();

//Display ImGUI basic toolbar
void			displayGui();

//Loads media mp3 , images e.t.c
bool			load_media();

//Event Handler
bool			event_handler(SDL_Event* event);

//Frees media and shuts down SDL2
void			close();

void            initCube();
void            displayCube();
void            colorcube();
void            quad(int a, int b, int c, int d);


//Global Variables
const int			SCREEN_WIDTH = 1024;
const int			SCREEN_HEIGHT = 780;
SDL_Window			*gWindow = NULL;
SDL_GLContext		gContext;

//The music that will be played
Mix_Music *gMusic = NULL;

//Simple Color for clearing - ImGui
ImVec4 clear_color = ImColor(0, 0, 128);
// flag for checkbox - ImGui
static bool checkbox = false;

GLuint      program;
GLuint      vao;
GLuint      buffer;
bool        wireFrame = false;
typedef     glm::vec4   color4;
typedef     glm::vec4   point4;
int                     Index = 0;
const       int         NumVertices = 36; //(6 faces)(2 triangles/face)(3 vertices/triangle)

										  
//arrays and global sliders
point4      points[NumVertices];
color4      colors[NumVertices];
glm::vec3   normals[NumVertices];
glm::mat4	proj_mat;
glm::mat4	modelview_mat;
GLuint		ProjMat;
GLuint		ModelViewMat;
GLuint		AmbientProduct;
GLuint		DiffuseProduct;
GLuint		SpecularProduct;
GLuint		LightPositions[5];
//Mesh object
Mesh	*m;
GLfloat		shine;


class Lights
{
public:
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position [4];
	GLfloat	Intensity;

	Lights() {
		this->ambient[0] = 1.0;
		this->ambient[1] = 0.0;
		this->ambient[2] = 0.0;
		this->ambient[3] = 1.0;

		this->diffuse[0] = 1.0f;
		this->diffuse[1] = 0.0f;
		this->diffuse[2] = 0.0f;
		this->diffuse[3] = 1.0f;

		this->specular[0] = 1.0;
		this->specular[1] = 0.0f;
		this->specular[2] = 0.0f;
		this->specular[3] = 1.0f;

		this->position[0] = 1.0f;
		this->position[1] = 2.0f;
		this->position[2] = 3.0f;
		this->position[3] = 1.0f;

		this->Intensity = 5.f;
	}
};

class MaterialProperties
{
public:
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat shininess;

	MaterialProperties() {
		this->ambient[0] = 0.2f;
		this->ambient[1] = 0.2f;
		this->ambient[2] = 0.2f;
		this->ambient[3] = 1.0f;

		this->diffuse[0] = 1.0f;
		this->diffuse[1] = 0.0f;
		this->diffuse[2] = 0.0f;
		this->diffuse[3] = 1.0f;

		this->specular[0] = 1.0f;
		this->specular[1] = 0.0f;
		this->specular[2] = 0.0f;
		this->specular[3] = 1.0f;
	
		this->shininess = 100.f;
	}
};

//Translation sliders
static float slider_t_x = 0.0f;
static float slider_t_y = 0.0f;
static float slider_t_z = 0.0f;
//Rotation sliders
static float slider_r_x = 0.0f;
static float slider_r_y = 0.0f;
static float slider_r_z = 0.0f;
//Scaling sliders
static float slider_s_x = 1.0f;
static float slider_s_y = 1.0f;
static float slider_s_z = 1.0f;
//Field-of-View slider
static float slider_fov = 45.0;
//Camera sliders
static float slider_camera_x = 2.0f;
static float slider_camera_y = 2.0f;
static float slider_camera_z = 5.0f;



Lights *light = new Lights();
MaterialProperties *cube = new MaterialProperties();

// Vertices of a unit cube centered at origin, sides aligned with axes
point4 vertices[8] = {
	point4(-0.5, -0.5, 0.5, 1.0),
	point4(-0.5, 0.5, 0.5, 1.0),
	point4(0.5, 0.5, 0.5, 1.0),
	point4(0.5, -0.5, 0.5, 1.0),
	point4(-0.5, -0.5, -0.5, 1.0),
	point4(-0.5, 0.5, -0.5, 1.0),
	point4(0.5, 0.5, -0.5, 1.0),
	point4(0.5, -0.5, -0.5, 1.0)
};

// RGBA olors
color4 vertex_colors[8] = {
	color4(0.0, 0.0, 0.0, 1.0),  // black
	color4(1.0, 0.0, 0.0, 1.0),  // red
	color4(1.0, 1.0, 0.0, 1.0),  // yellow
	color4(0.0, 1.0, 0.0, 1.0),  // green
	color4(0.0, 0.0, 1.0, 1.0),  // blue
	color4(1.0, 0.0, 1.0, 1.0),  // magenta
	color4(1.0, 1.0, 1.0, 1.0),  // white
	color4(0.0, 1.0, 1.0, 1.0)   // cyan
};

bool	init()
{
	//Init flag
	bool success = true;

	//Basic Setup
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) != 0)
	{
		std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		std::cout << std::endl << "Yay! Initialized SDL succesfully!" << std::endl;
		//Use OpenGL Core 3.2
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		//SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		//SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

		//Initialize SDL_mixer
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
			success = false;
		}

		success = load_media();

		//Create Window
		SDL_DisplayMode current;
		SDL_GetCurrentDisplayMode(0, &current);

		gWindow = SDL_CreateWindow("ImGui + SDL2 + OpenGL4 example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
		if (gWindow == NULL)
		{
			std::cout << "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			std::cout << std::endl << "Yay! Created window sucessfully!" << std::endl << std::endl;
			//Create context
			gContext = SDL_GL_CreateContext(gWindow);
			if (gContext == NULL)
			{
				std::cout << "OpenGL context could not be created! SDL Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					std::cout << "Error initializing GLEW! " << glewGetErrorString(glewError) << std::endl;
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					std::cout << "Warning: Unable to set Vsync! SDL Error: " << SDL_GetError() << std::endl;
				}

				//Initializes ImGui
				if (!initImGui())
				{
					std::cout << "Error initializing ImGui! " << std::endl;
					success = false;
				}

			}
		}
	}
	return success;
}

void	close()
{
	//Free the music
	Mix_FreeMusic(gMusic);
	gMusic = NULL;

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();

	SDL_GL_DeleteContext(gContext);
	SDL_DestroyWindow(gWindow);
	Mix_Quit();
	SDL_Quit();
}

bool	load_media()
{
	bool success = true;
	//Load music
	gMusic = Mix_LoadMUS("bensound-cute.mp3");
	if (gMusic == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}
	return success;
}

bool	event_handler(SDL_Event* event)
{
	switch (event->type)
	{
	case SDL_MOUSEWHEEL:
	{
		return true;
	}
	case SDL_MOUSEBUTTONDOWN:
	{
		if (event->button.button == SDL_BUTTON_LEFT)
			if (event->button.button == SDL_BUTTON_RIGHT)
				if (event->button.button == SDL_BUTTON_MIDDLE)
					return true;
	}
	case SDL_TEXTINPUT:
	{
		return true;
	}
	case SDL_KEYDOWN:
	{
		if (event->key.keysym.sym == SDLK_w)
		{
			if (wireFrame)
			{
				wireFrame = false;
			}
			else
			{
				wireFrame = true;
			}
		}
		if (event->key.keysym.sym == SDLK_9)
		{
			//If there is no music playing
			if (Mix_PlayingMusic() == 0)
			{
				//Play the music
				Mix_PlayMusic(gMusic, -1);
			}
			//If music is being played
			else
			{
				//If the music is paused
				if (Mix_PausedMusic() == 1)
				{
					//Resume the music
					Mix_ResumeMusic();
				}
				//If the music is playing
				else
				{
					//Pause the music
					Mix_PauseMusic();
				}
			}
			break;
		}
		if (event->key.keysym.sym == SDLK_0)
		{
			//Stop the music
			Mix_HaltMusic();
			break;
		}
		return true;
	}
	case SDL_KEYUP:
	{
		return true;
	}
	}
	return false;
}

// quad generates two triangles for each face and assigns colors
//    to the vertices
void	quad(int a, int b, int c, int d)
{
	colors[Index] = vertex_colors[a]; points[Index] = vertices[a]; Index++;
	colors[Index] = vertex_colors[b]; points[Index] = vertices[b]; Index++;
	colors[Index] = vertex_colors[c]; points[Index] = vertices[c]; Index++;
	colors[Index] = vertex_colors[a]; points[Index] = vertices[a]; Index++;
	colors[Index] = vertex_colors[c]; points[Index] = vertices[c]; Index++;
	colors[Index] = vertex_colors[d]; points[Index] = vertices[d]; Index++;
}

// generate 12 triangles: 36 vertices and 36 colors
void	colorcube()
{
	quad(1, 0, 3, 2);
	quad(2, 3, 7, 6);
	quad(3, 0, 4, 7);
	quad(6, 5, 1, 2);
	quad(4, 5, 6, 7);
	quad(5, 4, 0, 1);
}

void	initCube()
{
	

	//colorcube();
	//Load cube 
	m = new Mesh();
	m->loadMesh("sphere.dae");
	
	//generate and bind a VAO for the 3D axes
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//Load Shaders
	program = LoadShaders("vshaderCube.vert", "fshaderCube.frag");

	glUseProgram(program);

	// Create and initialize two buffer array object on the server side (GPU)
	GLuint buffer_array[3];
	glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(buffer_array), buffer_array);
	

	// Vertex VBO
	glBindBuffer(GL_ARRAY_BUFFER, buffer_array[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(m->Positions[0]) * m->Positions.size(), &m->Positions[0], GL_STATIC_DRAW);
	// Connect vertex arrays to the the shader attributes: vPosition, vNormal, vTexCoord
	GLuint vPositionMesh = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(vPositionMesh);
	glVertexAttribPointer(vPositionMesh, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	//stop using previous VBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	// Normal VBO
	glBindBuffer(GL_ARRAY_BUFFER, buffer_array[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(m->Normals[0]) * m->Normals.size(), &m->Normals[0], GL_STATIC_DRAW);
	GLuint vNormalMesh = glGetAttribLocation(program, "vNormal");
	glEnableVertexAttribArray(vNormalMesh);
	glVertexAttribPointer(vNormalMesh, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	//stop using previous VBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Index VBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer_array[2]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(m->Indices[0]) * m->Indices.size(), &m->Indices[0], GL_STATIC_DRAW);

	//Projection uniform
	ProjMat = glGetUniformLocation(program, "project");

	//Transform and View uniform
	ModelViewMat = glGetUniformLocation(program, "modelView");

	LightPositions[0] = glGetUniformLocation(program, "LightPosition");
	AmbientProduct = glGetUniformLocation(program, "AmbientProduct");
	DiffuseProduct = glGetUniformLocation(program, "DiffuseProduct");
	SpecularProduct = glGetUniformLocation(program, "SpecularProduct");

	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0, 0.0, 0.0, 1.0);


	// only one VAO can be bound at a time, so disable it to avoid altering it accidentally
	glBindVertexArray(0);
}

glm::mat4 rotateWhole() {
	glm::mat4 rotate_X = glm::rotate(glm::mat4(1.0), slider_r_x, glm::vec3(1, 0, 0));
	glm::mat4 rotate_Y = glm::rotate(glm::mat4(1.0), slider_r_y, glm::vec3(0, 1, 0));
	glm::mat4 rotate_Z = glm::rotate(glm::mat4(1.0), slider_r_z, glm::vec3(0, 0, 1));

	return rotate_X * rotate_Y * rotate_Z;
}

void	displayCube()
{
	glUseProgram(program);
	glBindVertexArray(vao);

	glDisable(GL_CULL_FACE);
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	if (wireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	

	//Translation Transformation
	glm::mat4 translate_mat = glm::translate(glm::mat4(1.0), glm::vec3(slider_t_x, slider_t_y, slider_t_z));
	//Rotation Transformation
	glm::mat4 rotate_mat = rotateWhole();
	//Scaling Transformation
	glm::mat4 scale_mat = glm::scale(glm::mat4(1.0), glm::vec3(slider_s_x, slider_s_y, slider_s_z));
	glm::mat4 model_mat = translate_mat * rotate_mat * scale_mat;
	//Camera
	glm::mat4 view_mat = glm::lookAt(glm::vec3(slider_camera_x, slider_camera_y, slider_camera_z), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glUniformMatrix4fv(ModelViewMat, 1, GL_FALSE, glm::value_ptr(view_mat * model_mat));

	//Projection
	proj_mat = glm::perspective(slider_fov , (float) (SCREEN_WIDTH /SCREEN_HEIGHT), 0.1f, 100.f);
	glUniformMatrix4fv(ProjMat, 1, GL_FALSE, glm::value_ptr(proj_mat));
	

	//Light position
	glUniform4fv(LightPositions[0], 1, glm::value_ptr(glm::vec4(light->position[0], light->position[1], light->position[2], light->position[3])));

	//Ambient product
	//std::cout << "Light position: x = "  << light->position[0]  <<" y =  " <<light->position[1]  <<" z = " <<light->position[2] << " w = "  <<light->position[3]  <<std::endl;
	//std::cout << "Light ambient: x = " <<light->ambient[0] << " y =  " << light->ambient[1] << " z = " << light->ambient[2] << " w = " << light->ambient[3] <<std::endl;
	
	glm::vec4 AmbientProductTemp = glm::vec4(cube->ambient[0], cube->ambient[1], cube->ambient[2], cube->ambient[3]) * glm::vec4(light->ambient[0], light->ambient[1], light->ambient[2], light->ambient[3]);
	std::cout << "ambient product: x = " << AmbientProductTemp.x << " y =  " << AmbientProductTemp.y << " z = " << AmbientProductTemp.z << " w = " << AmbientProductTemp.w << std::endl;
	glUniform4fv(AmbientProduct, 1, glm::value_ptr(AmbientProductTemp));

	//Diffuse product
	glm::vec4 DiffuseProductTemp = glm::vec4(cube->diffuse[0], cube->diffuse[1], cube->diffuse[2], cube->diffuse[3]) * glm::vec4(light->diffuse[0], light->diffuse[1], light->diffuse[2], light->diffuse[3]);
	std::cout << "diffuse product: x = " << DiffuseProductTemp.x << " y =  " << DiffuseProductTemp.y << " z = " << DiffuseProductTemp.z << " w = " << DiffuseProductTemp.w << std::endl;
	glUniform4fv(DiffuseProduct, 1, glm::value_ptr(DiffuseProductTemp));

	//Specular product
	glm::vec4 SpecularProductTemp = glm::vec4(cube->specular[0], cube->specular[1], cube->specular[2], cube->specular[3]) * glm::vec4(light->specular[0], light->specular[1], light->specular[2], light->specular[3]);
	std::cout << "specular product: x = " << DiffuseProductTemp.x << " y =  " << DiffuseProductTemp.y << " z = " << DiffuseProductTemp.z << " w = " << DiffuseProductTemp.w << std::endl;
	glUniform4fv(SpecularProduct, 1, glm::value_ptr(SpecularProductTemp));

	//Shininess
	glUniform1fv(glGetUniformLocation(program, "Shininess"), 1, &(cube->shininess));

	//Intensity
	glUniform1fv(glGetUniformLocation(program, "Intensity"), 1, &(light->Intensity));

	m->render();

	glPopAttrib();
	glBindVertexArray(0);
}

bool	initImGui()
{
	IMGUI_CHECKVERSION();
	ImGui::SetCurrentContext(ImGui::CreateContext());

	// Setup ImGui binding

	if (!ImGui_ImplOpenGL3_Init("#version 150"))
	{
		return false;
	}
	if (!ImGui_ImplSDL2_InitForOpenGL(gWindow, gContext) )
	{
		return false;
	}

	//ImGui::StyleColorsDark();

	// Load Fonts
	// (there is a default font, this is only if you want to change it. see extra_fonts/README.txt for more details)

	// Marios -> in order to use custom Fonts, 
	//there is a file named extra_fonts inside /_thirdPartyLibs/include/ImGUI/extra_fonts
	//Uncomment the next line -> ImGui::GetIO() and one of the others -> io.Fonts->AddFontFromFileTTF("", 15.0f). 
	//Important : Make sure to check the first parameter is the correct file path of the .ttf or you get an assertion.

	//ImGuiIO& io = ImGui::GetIO();
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("../../extra_fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../extra_fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../extra_fonts/ProggyClean.ttf", 13.0f);
	//io.Fonts->AddFontFromFileTTF("../../extra_fonts/ProggyTiny.ttf", 10.0f);
	//io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());

	return true;
}

void	displayGui()
{
	// 1. Show a simple window
	// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"

	//Sets the Window size
	ImGui::SetNextWindowSize(ImVec2(400, 160), ImGuiSetCond_FirstUseEver);
	ImGui::SetNextWindowPos(ImVec2(10, 0));
	ImGui::Begin("basicCube GUI");
	

	//Translation Coordinates
	ImGui::Text("Translation Coordinates");

	// creates a slider for x translation
	ImGui::SliderFloat("Translate X", &slider_t_x, -1.0f, 1.0f);
	// creates a slider for y translation
	ImGui::SliderFloat("Translate Y", &slider_t_y, -1.0f, 1.0f);
	// creates a slider for z translation
	ImGui::SliderFloat("Translate Z", &slider_t_z, -1.0f, 1.0f);

	// puts a line between widgets
	ImGui::Separator();


	//Roatation Coordinates
	ImGui::Text("Roatation Coordinates");
	// creates a slider for x rotation
	ImGui::SliderFloat("Rotate X", &slider_r_x, -1.0f, 1.0f);
	// creates a slider for y rotation
	ImGui::SliderFloat("Rotate Y", &slider_r_y, -1.0f, 1.0f);
	// creates a slider for x rotation
	ImGui::SliderFloat("Rotate Z", &slider_r_z, -1.0f, 1.0f);

	ImGui::Separator();


	//Scaling Coordinates
	ImGui::Text("Scaling Coordinates");
	// creates a slider for x scaling
	ImGui::SliderFloat("Scale X", &slider_s_x, 0.0f, 2.0f);
	// creates a slider for y scaling
	ImGui::SliderFloat("Scale Y", &slider_s_y, 0.0f, 2.0f);
	// creates a slider for x scaling
	ImGui::SliderFloat("Scale Z", &slider_s_z, 0.0f, 2.0f);

	ImGui::Separator();
	//FoV.
	ImGui::SliderFloat("Field of View", &slider_fov, 45.f , 90.f);
	ImGui::Separator();

	//Camera position
	ImGui::SliderFloat("Camera X", &slider_camera_x, -10.f, 10.f);
	ImGui::SliderFloat("Camera Y", &slider_camera_y, -10.f, 10.f);
	ImGui::SliderFloat("Camera Z", &slider_camera_z, -10.f, 10.f);

	ImGui::Separator();

	

	ImGui::Separator();
	
//ImGui::SliderFloat("Ambient X", &slider_light_ambient_x, 0.f, 10.f);
	//ImGui::SliderFloat("Ambient Y", &slider_light_ambient_y, -10.f, 10.f);
	//ImGui::SliderFloat("Ambient Z", &slider_light_ambient_z, -10.f, 10.f);
	//ImGui::SliderFloat("Ambient W", &slider_light_ambient_w, -10.f, 10.f);

	//ImGui::Separator();
	//Light Properties - Diffuse Color
	//ImGui::SliderFloat("Diffuse X", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Diffuse Y", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Diffuse Z", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Diffuse W", &slider_camera_y, -10.f, 10.f);

	//ImGui::Separator();
	//Light Properties - Specular Color
	//ImGui::SliderFloat("Specular X", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Specular Y", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Specular Z", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Specular W", &slider_camera_y, -10.f, 10.f);

	//ImGui::Separator();
	//Material Properties - Ambient
	//ImGui::SliderFloat("Ambient X", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Ambient Y", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Ambient Z", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Ambient W", &slider_camera_y, -10.f, 10.f);

	//ImGui::Separator();
	//Material Properties - Diffuse Color
	//ImGui::SliderFloat("Diffuse X", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Diffuse Y", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Diffuse Z", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Diffuse W", &slider_camera_y, -10.f, 10.f);

	//ImGui::Separator();
	//Material Properties - Specular Color
	//ImGui::SliderFloat("Specular X", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Specular Y", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Specular Z", &slider_camera_y, -10.f, 10.f);
	//ImGui::SliderFloat("Specular W", &slider_camera_y, -10.f, 10.f);

	//ImGui::Separator();
	// manipulates colors
	//ImGui::ColorEdit3("clear color", (float*)&clear_color);
	//ImGui::Separator();


	// creates a checkbox
	if (ImGui::Checkbox("Wireframe", &checkbox))
	{
		if (checkbox == false)
		{
			wireFrame = false;
		}

		if (checkbox == true)
		{
			wireFrame = true;
		}
	}



	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::End();

	ImGui::Begin("Lights-Materials");
	ImGui::SetWindowPos(ImVec2(10, 220), ImGuiSetCond_Once);
	ImGui::SetWindowSize(ImVec2(200, 200), ImGuiSetCond_Once);
	if (ImGui::TreeNode("Edit Lights"))
	{
		if (ImGui::TreeNode("Light #1"))
		{
			ImGui::ColorEdit3("Ambient", light->ambient);
			ImGui::ColorEdit3("Diffuse", light->diffuse);
			ImGui::ColorEdit3("Specular", light->specular);
			if (ImGui::TreeNode("Position"))
			{
				//Light Properties - Position
				ImGui::SliderFloat("Position X", &light->position[0], -10.f, 10.f);
				ImGui::SliderFloat("Position Y", &light->position[1], -10.f, 10.f);
				ImGui::SliderFloat("Position Z", &light->position[2], -10.f, 10.f);
				ImGui::SliderFloat("Position W", &light->position[3], 0.f, 1.f);
				ImGui::TreePop();
			}
			//Light Properties - Intensity
			ImGui::SliderFloat("Intensity", &light->Intensity, 0.f, 2.f);
			ImGui::TreePop();
		}

		if (ImGui::TreeNode("Material Properties"))
		{
			ImGui::ColorEdit3("Ambient", cube->ambient);
			ImGui::ColorEdit3("Diffuse", cube->diffuse);
			ImGui::ColorEdit3("Specular", cube->specular);
			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
	ImGui::End();
}


int main(int, char**)
{
	int running = true;

	if (init() == false)
	{
		std::cout << "Init failed !!!" << std::endl;
		exit(EXIT_FAILURE);
	}

	initCube();

	// Main loop
	while (running)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			ImGui_ImplSDL2_ProcessEvent(&event);

			//our custom event_handler
			event_handler(&event);
			if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
			{
				running = GL_FALSE;
			}
			if (event.type == SDL_QUIT)
				running = false;

		}

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame(gWindow);
		ImGui::NewFrame();

		// Rendering
		//glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
		glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		displayCube();

		//However displayCube is rendered , we want ImGui to be filled
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		displayGui();

		//Most needed ImGui function 
		//Must be called just before SDL_GL_SwapWindow
		ImGui::Render();
		SDL_GL_MakeCurrent(gWindow, gContext);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());


		SDL_GL_SwapWindow(gWindow);
	}

	close(); //Shuts down every little thing...
	return 0;
}